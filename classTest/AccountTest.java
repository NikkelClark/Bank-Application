import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AccountTest {

    private Account classTest1, classTest2, classTest3;
    private ArrayList<Account> databaseRef;

    @Before
    public void setUp() throws Exception {
        databaseRef = new ArrayList<Account>();

        classTest1 = new Account(1, databaseRef);
        classTest2 = new Account(2, databaseRef);
        classTest3 = new Account(3, databaseRef);

        databaseRef.add(classTest1);
        databaseRef.add(classTest2);
        databaseRef.add(classTest3);
    }

    @Test
    public void depositFunds() {
        classTest1.depositFunds(50.00);
        classTest1.depositFunds(50.00);
        assertEquals(100.00, classTest1.getBalance(), .0);
    }

    @Test
    public void withdrawFunds() {
        classTest2.depositFunds(50.00);
        classTest2.withdrawFunds(25.50);
        assertEquals(24.50, classTest2.getBalance(), .0);
    }

    @Test
    public void transferFunds() {
        classTest1.depositFunds(150.00);
        classTest2.depositFunds(50);
        classTest1.transferFunds(2, 50);
        assertEquals(100, classTest1.getBalance(), .0);
        assertEquals(100, classTest2.getBalance(), .0);
    }

    @Test
    public void printAccountStatments() {
        classTest1.depositFunds(40);

        classTest3.depositFunds(400.42);
        classTest3.transferFunds(1, 59.34);
        classTest3.withdrawFunds(214.87);
        classTest1.transferFunds(3, 24);
        classTest3.depositFunds(76);
        classTest1.transferFunds(3,36);
        classTest3.printAccountStatements();

        //TODO Fix the value that stores money
        assertEquals(262.21, classTest3.getBalance(), 0);
    }
}