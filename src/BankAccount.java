
public interface BankAccount {
    public void depositFunds(double amount);
    public void withdrawFunds(double amount);
    public void transferFunds(int toAccountNumber, double amount);
    public boolean receiveFunds(double amount);
    public void printAccountStatements();
}
