import Statements.Action;
import Statements.Statments;

import java.util.ArrayList;
import java.util.List;

public class Account implements BankAccount {
    private ArrayList<Account> refDatabase;
    private int accountNumber;
    private double balance;
    private Statments statments;

    public Account(int accountNumber, ArrayList<Account> e) {
        this.refDatabase = e;
        this.accountNumber = accountNumber;
        this.statments = new Statments();
    }

    public void depositFunds(double amount) {
        this.balance += amount;
        this.createNewTransaction(Action.DEPOSIT, amount, this.balance);
        System.out.println("Successfully deposited £"+amount);
    }

    public void withdrawFunds(double amount) {
        if(amount <= balance) {
            this.balance -= amount;
            this.createNewTransaction(Action.WITHDRAW, amount, this.balance);
            System.out.println("Successfully withdrawn £"+amount);
        } else {
            System.out.println("Don't have enough funds");
        }
    }

    public void transferFunds(int toAccountNumber, double amount) {
        if(amount <= balance) {
            this.balance -= amount;
            transferFundsByID(toAccountNumber, amount);
            this.createNewTransaction(Action.TRANSFER, amount, this.balance);
            System.out.println("Successfully transferred £"+amount+" to "+toAccountNumber);
        } else {
            System.out.println("Don't have enough funds");
        }
    }

    public boolean receiveFunds(double amount) {
        this.balance += amount;
        this.createNewTransaction(Action.TRANSFER_DEPOSIT, amount, this.balance);
        return true;
    }

    private boolean transferFundsByID(int toAccountNumber, double amount) {
        for(int i = 0; i < refDatabase.size(); i++) {
            if(refDatabase.get(i).accountNumber == toAccountNumber)
                return refDatabase.get(i).receiveFunds(amount);
        }
        return false;
    }

    //TODO Complete the print statement with filters
    public void printAccountStatements() {
        System.out.println("Statement for Account Number: " + accountNumber);
        statments.getStatment();
    }

    private void createNewTransaction(Action action, double amount, double balance) {
        this.statments.createNewTransaction(action, amount, balance);
    }

    //TODO return to two decimal places or ensure it's stored to two decimal places
    protected double getBalance()
    {
        return balance;
    }
}