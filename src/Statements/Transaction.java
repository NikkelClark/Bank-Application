package Statements;

import java.util.Date;

public class Transaction {
    private int id;
    private Action action;
    private double amount,balance;
    private Date date;

    public Transaction(int id,Action action, double amount, double balance) {
        this.id = id;
        this.action = action;
        this.amount = amount;
        this.balance = balance;
        this.date = new Date();
    }

    public int getId() {
        return id;
    }

    public Action getAction() {
        return action;
    }

    public double getAmount() {
        return amount;
    }

    public double getBalance() {
        return balance;
    }

    public Date getDate() {
        return date;
    }
}
