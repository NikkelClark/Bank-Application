package Statements;

public enum Action {
    DEPOSIT, //+  0
    WITHDRAW, //- 1
    TRANSFER, //- 2
    TRANSFER_DEPOSIT //+ 3
}
