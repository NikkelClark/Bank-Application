package Statements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Statments {
    private ArrayList<Transaction> statments;

    public Statments() {
        statments = new ArrayList<Transaction>();
    }

    public void createNewTransaction(Action action, double amount, double balance)
    {
        statments.add(new Transaction(statments.size(), action, amount, balance));
    }

    public void getStatment() {
        System.out.println("Date | Amount | Balance");
        SimpleDateFormat display = new SimpleDateFormat("dd/MM/yyyy");
        for (Transaction e : statments) {
            System.out.println(display.format(e.getDate())+" | "+e.getAction().toString()+": £"+e.getAmount()+" | £"+e.getBalance());
        }
    }
}